fetch("http://localhost:8080/cards")
  .then((response) => {
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    return response.json();
  })
  .then((data) => {
    // Créer une carte avec les données obtenues de l'API
    data.forEach((item) => {
      createCard(item.question, item.answer, item.id);
    });
  })
  .catch((error) => {
    console.error("There was a problem with the fetch operation:", error);
  });

document.getElementById("search").addEventListener("submit", (event) => {
  event.preventDefault();
  var cardContainer = document.querySelector(".card-container");
  cardContainer.innerHTML = "";
  var input = document.getElementById("search-input").value;
  if (input !== "") {
    fetch(`http://localhost:8080/cards?tags=${input}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        // Créer une carte avec les données obtenues de l'API
        data.forEach((item) => {
          createCard(item.question, item.answer, item.id);
        });
      })
      .catch((error) => {
        console.error("There was a problem with the fetch operation:", error);
      });
  }
});

// Fonction pour créer une carte et l'ajouter au conteneur
function createCard(question, answer, id) {
  var template = document.querySelector(".card-template");
  var cardContainer = document.querySelector(".card-container");
  // cardContainer.innerHTML = "";

  // Cloner le modèle de carte
  var cardClone = template.cloneNode(true);
  cardClone.querySelector(".question").innerText = "Question : " + question;
  cardClone.querySelector(".answer").innerText = "Réponse : " + answer;
  cardClone.classList.add("open"); // Ajouter la classe 'open' pour afficher la réponse
  cardClone.id = id;

  // Afficher la carte dans le conteneur
  cardContainer.appendChild(cardClone);
  cardClone.style.display = "block"; // Afficher la carte clonée
}

// Exemple de données d'une carte obtenues depuis l'API
// var exempleQuestion = "Quelle est la capitale de la France ?";
// var exempleAnswer = "La capitale de la France est Paris.";

// Fonction pour retourner la carte
function flipCard(button) {
  var card = button.closest(".card-template");
  var question = card.querySelector(".question").innerHTML;
  var answer = card.querySelector(".answer").innerHTML;
  const parts = answer.split(":");
  const desiredPart = parts[1].trim();

  // card.classList.toggle("open");
  localStorage.setItem("question", question);
  localStorage.setItem("answer", desiredPart);
  localStorage.setItem("id", card.id);
  window.location.href = "../Views/FlashcardQuiz.html";
}
