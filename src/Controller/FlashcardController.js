class QuestionHandler {
  constructor() {
    this.questionInput = document.getElementById("question");
    this.reponseInput = document.getElementById("reponse");
    this.initEventListeners();
  }

  initEventListeners() {
    // méthode pour initialiser les écouteurs d'événements
    const creationButton = document.getElementById("btnCreationQuestion");
    // Empêcher le formulaire de soumettre et de rafraîchir la page
    creationButton.addEventListener("click", (e) => {
      e.preventDefault();
      this.handleQuestionCreation();
    });
  }

  handleQuestionCreation() {
    // méthode pour gérer la création de question
    const questionValue = this.questionInput.value;
    if (questionValue) {
      // Traiter la question ici (par exemple, enregistrer ou afficher)
      // console.log('Question créée:', questionValue);
      // Vous pouvez ajouter des actions supplémentaires ici, comme envoyer la question à un serveur, etc.
    } else {
      console.log("La question est vide.");
    }
  }
}

// Initialiser la classe lorsque le DOM est prêt
document.addEventListener("DOMContentLoaded", () => {
  const questionHandler = new QuestionHandler();
});
