class QuestionReponseController {
  constructor() {
    this.initEventListeners();
  }

  initEventListeners() {
    // Écouteur d'événement pour le bouton de création de question
    document
      .getElementById("btnCreationQuestion")
      .addEventListener("click", (e) => {
        e.preventDefault(); // Empêche le comportement par défaut du formulaire
        this.creerQuestion();
      });
  }

  creerQuestion() {
    const question = document.getElementById("question").value;
    const reponse = document.getElementById("reponse").value;
    const tag = document.getElementById("tag").value;
    const data = {
      question: question,
      answer: reponse,
      tag: tag,
    };

    this.envoyerQuestionReponse(data);
  }

  async envoyerQuestionReponse(data) {
    try {
      const reponse = await fetch("http://localhost:8080/cards", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      if (!reponse.ok) {
        throw new Error(
          "Erreur lors de l'envoi de la question et de la réponse"
        );
      }

      const contenuReponse = await reponse.json();
      openModal();
      // Traiter ici la réponse de l'API, par exemple en affichant un message de succès
    } catch (erreur) {
      console.error(
        "Erreur lors de l'envoi de la question et de la réponse:",
        erreur
      );
      // Traiter ici l'erreur, par exemple en affichant un message d'erreur
    }
  }
}

function openModal() {
  var modal = new bootstrap.Modal(document.getElementById("modal"));
  modal.show();
}

// Instanciation de la classe au chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  new QuestionReponseController();
});
