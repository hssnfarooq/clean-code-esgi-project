class QuizCarte {
  constructor(reponseAPI) {
    this.question = reponseAPI.question;
    this.reponseCorrecte = reponseAPI.reponse;
    this.id = reponseAPI.id;
    this.initialiser();
  }

  initialiser() {
    // Injecte la question dans l'ID 'question'
    document.getElementById("question").textContent = this.question;

    // Ajoute un écouteur pourr 'btnEnvoieReponse'
    document
      .getElementById("btnEnvoieReponse")
      .addEventListener("click", (event) => {
        event.preventDefault();
        this.evaluerReponse();
      });
  }

  evaluerReponse() {
    // Récupére la réponse de l'utilisateur
    const reponseUtilisateur = document.getElementById("reponse").value;

    // Compare la réponse de l'utilisateur avec la bonne réponse
    if (
      reponseUtilisateur.trim().toLowerCase() ===
      this.reponseCorrecte.trim().toLowerCase()
    ) {
      // La réponse est correcte
      this.envoyerResultat(true);
    } else {
      // La réponse est incorrecte
      openModal("modal2");
      document
        .getElementById("force-validate")
        .addEventListener("click", () => {
          this.envoyerResultat(false);
        });
    }
  }

  envoyerResultat(estCorrect) {
    // Envoyer le résultat à l'API
    if (estCorrect === true) {
      fetch(`http://localhost:8080/cards/${this.id}/answer`, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          question: this.question,
          isValid: estCorrect,
        }),
      })
        .then((reponse) => reponse.json())
        .then((donnees) => {
          document.querySelector("#modal-text-success").innerHTML =
            donnees.reponse || donnees.error;
          openModal("modal1");
          console.log("Succès:", donnees);
        })
        .catch((erreur) => {
          document.querySelector("#modal-text-fail").innerHTML =
            "Erreur: " + erreur.error;
          openModal("modal2");
          console.error("Erreur:", erreur);
        });
    } else {
      fetch(`http://localhost:8080/cards/${this.id}/answer/force`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          question: this.question,
          isValid: estCorrect,
        }),
      })
        .then((reponse) => reponse.json())
        .then((donnees) => {
          document.querySelector("#modal-text-success").innerHTML =
            donnees.reponse || donnees.error;
          openModal("modal1");
          console.log("Succès:", donnees);
        })
        .catch((erreur) => {
          document.querySelector("#modal-text-fail").innerHTML =
            "Erreur: " + erreur.error;
          openModal("modal2");
          console.error("Erreur:", erreur);
        });
    }
  }
}

const reponseAPI = {
  question: localStorage.getItem("question"),
  reponse: localStorage.getItem("answer"),
  id: localStorage.getItem("id"),
};

function openModal(id) {
  var modal = new bootstrap.Modal(document.getElementById(id));
  modal.show();
}

const quizCarte = new QuizCarte(reponseAPI);
// quizCarte.initialiser();
// quizCarte.evaluerReponse();
// quizCarte.envoyerResultat();
